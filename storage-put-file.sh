FILE=$*

if [ -f "$FILE" ]
  then printf "processing file $FILE\n" 
  else printf "no such file $FILE\n"; exit
fi

UUID=`uuidgen`
SHA1=`sha1sum "$FILE" | cut -c -40`
SHA1_SUBDIR=`printf $SHA1 | cut -c -2`
SHA1_FILE=`printf $SHA1 | cut -c 3-`
TIMESTAMP="["`date +%Y-%m-%d\ %a\ %H:%M`"]"
FILE_NAME=`printf "$FILE" | sed -r 's/.*\/(.*)/\1/'`

FILES_INDEX_FILE="$HOME/stg/htg-sct/index.org"
SHA1_STORAGE="$HOME/stg/htg-sct"

#check is sha1 storage file exists
if [ -f $SHA1_STORAGE/$SHA1_SUBDIR/$SHA1_FILE ]
  then 
    printf "file $FILE_NAME exists in sha1 storage at $SHA1_STORAGE/$SHA1_SUBDIR/$SHA1_FILE\n"
    exit
  else 
    printf "file $FILE_NAME doesn not exist in storage\n"
fi

#add file to index
if [ -z "`grep $SHA1 $FILES_INDEX_FILE`" ]
  then 
    printf "* $FILE_NAME\n   :PROPERTIES:\n   :CUSTOM_ID: $UUID\n   :SHA1: $SHA1\n   :CREATED: $TIMESTAMP\n   :END:\n" >> $FILES_INDEX_FILE
  else 
    printf "file with sha1 hashsum $SHA1 already in index\n"
    exit
fi

#create file in sha1 storage
if [ -d "$SHA1_STORAGE/$SHA1_SUBDIR" ]
  then 
    echo "subdir exists already"
  else 
    echo "subdir isn't exist. creating."
    mkdir $SHA1_STORAGE/$SHA1_SUBDIR
    chmod 500 $SHA1_STORAGE/$SHA1_SUBDIR
fi

#allow to write files to directory
chmod -v 700 $SHA1_STORAGE/$SHA1_SUBDIR
#copy file to sha1 storage
cp "$FILE" $SHA1_STORAGE/$SHA1_SUBDIR/$SHA1_FILE
#restrict changing the file
chmod -v 400 $SHA1_STORAGE/$SHA1_SUBDIR/$SHA1_FILE
#restrict writing
chmod -v 500 $SHA1_STORAGE/$SHA1_SUBDIR

echo $FILE 
echo $UUID
echo $SHA1
echo $SHA1_SUBDIR
echo $SHA1_FILE
echo $TIMESTAMP
echo $FILE_NAME

