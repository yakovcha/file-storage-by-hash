FILES_INDEX_FILE="$HOME/h/txt/files-index.org"
SHA1_STORAGE="$HOME/h/stg/sha1"
SHA1_IN_STORAGE="/tmp/sha1sums-in-storage-`date +%Y%m%d%H%M%S`"
SHA1_IN_INDEX="/tmp/sha1sums-in-file-index-`date +%Y%m%d%H%M%S`"

#check all files in storage have folder name + file name equal to file hash sum
#cd $SHA1_STORAGE
#du . -a | sed -rn 's/.*([0-9a-f]{2}\/[0-9a-f]{38})/\1/p' | xargs sha1sum | sed 's/\///' | awk '{ if ($1 != $2) printf "file name %s and sha1sum %s mismatch\n",$2,$1 }'

sed -rn 's/.*SHA1.*([0-9a-f]{40})/\1/p' $FILES_INDEX_FILE | sort > $SHA1_IN_INDEX
du $SHA1_STORAGE -a | sed -rn 's/.*([0-9a-f]{2})\/([0-9a-f]{38})/\1\2/p' | sort > $SHA1_IN_STORAGE

DIFFERENCE=`diff $SHA1_IN_INDEX $SHA1_IN_STORAGE`

if [ -z "$DIFFERENCE" ]
  then 
    printf "hashes in index and storage match\n"
  else 
    printf "hashes in index and storage mismatch\n"
    #TODO output some more verbose and understandable output
    printf "hashes in index\n"
    cat $SHA1_IN_INDEX
    printf "=====\n"
    printf "hashes in storage\n"
    cat $SHA1_IN_STORAGE
    printf "=====\n"
    printf "$DIFFERENCE\n"
fi

rm $SHA1_IN_INDEX $SHA1_IN_STORAGE

#TODO
#check for files in storage but not in index
#check for files in index but not in storage




