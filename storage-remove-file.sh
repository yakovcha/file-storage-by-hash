SHA1=$*
SHA1_SUBDIR=`printf $SHA1 | cut -c -2`
SHA1_FILE=`printf $SHA1 | cut -c 3-`

FILES_INDEX_FILE="$HOME/h/txt/files-index.org"
SHA1_STORAGE="$HOME/h/stg/sha1"

#TODO remove file information from index

#check is sha1 storage file exists
if [ -f $SHA1_STORAGE/$SHA1_SUBDIR/$SHA1_FILE ]
  then 
     printf "file $SHA1 exists in sha1 storage at $SHA1_STORAGE/$SHA1_SUBDIR/$SHA1_FILE\n"
     #allow to write files to directory
     chmod -v 700 $SHA1_STORAGE/$SHA1_SUBDIR
     #remove file from sha1 storage
     chmod -v 200 $SHA1_STORAGE/$SHA1_SUBDIR/$SHA1_FILE
     rm $SHA1_STORAGE/$SHA1_SUBDIR/$SHA1_FILE
     #restrict writing
     chmod -v 500 $SHA1_STORAGE/$SHA1_SUBDIR
     printf "File $SHA1 removed\n"
  else 
    printf "file $SHA1 doesn not exist in storage\n"
fi



